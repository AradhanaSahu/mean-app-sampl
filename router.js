(function() {

    function establishRoutes(app, helpers) {

        app.use('/me', function(req, res, next) {
            require('./api/me_get').execute(req, res, next, helpers);
        });
    }

    exports.establishRoutes = establishRoutes;
})()