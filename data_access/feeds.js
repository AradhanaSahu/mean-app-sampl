(function() {

    function getFeedsList(query, options, helpers, cb) {

        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Feeds = dbClient.collection('feeds');

                Feeds.find(query).toArray(function(retrievalErr, feeds) {
                    if (!retrievalErr) {
                        helpers.execute(cb, [feeds]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    exports.Feeds = {
        getFeedsList: getFeedsList
    }

})();