var app = require('express')()

var bodyparser = require('body-parser');

var cookieParser = require('cookie-parser');

var MongoClient = require('mongodb').MongoClient;

var dburl = "mongodb://localhost:27017/mean-sample-db";

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var session = require('express-session');

var morgan = require('morgan');

var validate_user_post = require('./api/validate_user_post')

var helpers = require('./helpers');

//body parser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

app.use(cookieParser());

app.use(session({
    secret: "hello",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: !true }
}));

app.use(morgan('dev'));



require('./router').establishRoutes(app, helpers);

app.listen(3000)
console.log('port number 3000 is running')