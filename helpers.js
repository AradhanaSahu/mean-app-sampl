(function() {

    var _mongoClient = null;

    function sendEmail() {

    }

    function execute(cb, params, onObj) {
        if (cb) {
            setImmediate(function() {
                try {
                    cb.apply(onObj, params);
                } catch (unhandledErr) {
                    console.error('Unhandled Error while setting Immediate', unhandledErr);
                }
            });
        }
    }

    function generateUUID() {
        return "SOME RANDOM STRING";
    }

    function getDbClient(cb) {
        if (!_mongoClient) {
            var db_url = "localhost:27017/mean-sample-db"
            if (db_url) {
                var MongoClient = require('mongodb').MongoClient;
                MongoClient.connect(db_url, function(err, db) {
                    if (err) {
                        console.error('Error retrieveing Mongo DB Client ' + err.toString());
                    } else {
                        _mongoClient = db;
                        _mongoClient.on('close', function() {
                            _mongoClient = null;
                        });
                    }
                    try {
                        cb(err, _mongoClient);
                    } catch (unhandledErr) {
                        console.error('Unhandled Error while returning connection from mongodb. Error: ', unhandledErr);
                    }
                });
            } else {
                console.error("Error: DB url not configured.");
            }
        } else {
            try {
                cb(null, _mongoClient);
            } catch (unhandledErr) {
                console.error('Unhandled Error while returning connection from mongodb. Error: ', unhandledErr);
            }
        }
    }

    exports.Helpers = {
        sendEmail: sendEmail,
        generateUUID: generateUUID,
        execute: execute,
        getDbClient: getDbClient
    }

})()