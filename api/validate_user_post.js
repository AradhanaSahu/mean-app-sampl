var express = require('express');
var passport = require('passport');
var  LocalStrategy = require('passport-local').Strategy;
var MongoClient = require('mongodb').MongoClient;
var bodyparser = require('body-parser');  
 var cookieParser = require('cookie-parser');
 var session = require('express-session');
// var flash = require('connect-flash');

 var app = express()


var router = express.Router();

app.use(bodyparser.json());
 app.use(bodyparser.urlencoded({extended:true}));
//cookie parser

app.use(cookieParser());

app.use(session({
    secret: "hello",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: !true }
}));

router.use(passport.initialize());
router.use(passport.session());
//app.use(flash());


//serializer
passport.serializeUser(function(user, done) {
   console.log('serialize User'+ JSON.stringify(user));
   done(null, user);
});
 
 //deserializer
passport.deserializeUser(function(user, done) {
  console.log('deserializer',user)
      done(null, user);
   });


passport.use(new LocalStrategy(
        function(username, password, done) {
            var url = 'mongodb://localhost:27017/customs';
            MongoClient.connect(url, (err, db) => {
                    if (err) {
                        throw err;
                    }
                db.collection('managers').find({ username: username }).toArray().then((user) => {
                        console.log(user);
                        if (err) { return done(err); }
                        if (user.length === 0) {
                            console.log('user not find');
                            return done(null, false, { message: 'Incorrect username.' });      
                        }
                        if (user[0].password !== password) {
                            console.log('password not matched');
                            return done(null, false, { message: 'Incorrect password.' });
                        }
                      console.log('success'+JSON.stringify(user[0]));
                      return done(null,user[0]);
                        }, (err) => {
                            throw err;
                        }); 
                   db.close();
                });
        })
);

router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/api/success',
        failureRedirect: '/api/login'
     //   failureFlash: true
    })
);

router.get('/success', (req, res) => {
  res.status(200).send('login successful');
});

router.get('/login', (req, res) => {
  res.status(401).send('Invalid');
});

module.exports = router;